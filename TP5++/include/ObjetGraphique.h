#ifndef OBJETGRAPHIQUE_H
#define OBJETGRAPHIQUE_H


class ObjetGraphique
{
    public:
        ObjetGraphique(int couleur = 0, int epaisseur = 1);
        virtual ~ObjetGraphique() = 0;

        int getCouleur() const;
        void setCouleur(int nouvelleCouleur);
        void setEpaisseur(int nouvelleEpaisseur);
        virtual void afficher() const = 0;
        virtual void effacer() const = 0;

    static int getNbObjetsGraphiques();

    protected:
        int couleur;
        int epaisseur;

    private:
        int NbObjetsGraphiques;

};

#endif // OBJETGRAPHIQUE_H
