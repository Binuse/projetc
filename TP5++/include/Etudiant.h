#include <iostream>
#include <string>
#include "personne.h"
#ifndef ETUDIANT_H
#define ETUDIANT_H


class Etudiant : public Personne
{
    public:
        // Constructeurs et destructeur
        Etudiant();
        Etudiant(const std::string& _nom, const std::string& _prenom, const std::string& _numeroINE);
        virtual ~Etudiant();

        void afficher() const override;
        // M�thodes pour lire et �crire les donn�es de l'objet
        void lire(std::istream& is);
        void ecrire(std::ostream& os) const;

    protected:

    private:
        //attribut numero ine d'un �tudiant
        std::string numeroINE;
};

#endif // ETUDIANT_H
