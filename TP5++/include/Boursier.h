#include <iostream>
#include <string>
#include "etudiant.h"
#ifndef BOURSIER_H
#define BOURSIER_H



class Boursier : public Etudiant {
    public:
        // Constructeurs et destructeur
        Boursier();
        Boursier(const std::string& _nom, const std::string& _prenom, const std::string& _numeroINE, const std::string& _numeroDossierBourse);
        virtual ~Boursier();

        void afficher();
        // M�thodes pour lire et �crire les donn�es de l'objet
        void lire(std::istream& is);
        void ecrire(std::ostream& os) const;

    protected:

    private:
        //attribut num�ro de dossier d'un boursier
        std::string numeroDossierBourse;
};

#endif // BOURSIER_H
