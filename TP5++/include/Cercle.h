#ifndef CERCLE_H
#define CERCLE_H


#include "ObjetGraphique.h"

class Cercle : public ObjetGraphique
{
    public:
        Cercle(int rayon = 0, int couleur = 0, int epaisseur = 1);
        virtual ~Cercle();
        int getRayon() const;
        void modifierRayon(int nouveauRayon);
        virtual void afficher() const override;
        virtual void effacer() const override;

    protected:

    private:
        int rayon;

};

#endif // CERCLE_H
