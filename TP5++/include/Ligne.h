#ifndef LIGNE_H
#define LIGNE_H

#include "ObjetGraphique.h"

class Ligne : public ObjetGraphique
{
    public:
        Ligne(int longueur = 0, double angle = 0.0, int couleur = 0, int epaisseur = 1);
        virtual ~Ligne();
        int getLongueur() const;
        double getAngle() const;
        void modifierLigne(int nouvelleLongueur, double nouvelAngle);
        virtual void afficher() const override;
        virtual void effacer() const override;
    protected:

    private:
        int longueur;
        double angle;
};

#endif // LIGNE_H
