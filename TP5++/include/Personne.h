#ifndef PERSONNE_H
#define PERSONNE_H
#include <string>

class Personne {

    public:

    // Constructeurs et destructeur
    Personne();
    Personne(const std::string& _nom, const std::string& _prenom);
    virtual ~Personne();

    // M�thode virtuelle pure � impl�menter dans les classes d�riv�es
    virtual void afficher() const;

    // M�thodes pour lire et �crire les donn�es de l'objet
    void lire(std::istream& is);
    void ecrire(std::ostream& os) const;

    // Op�rateur de flux d'entr�e
    friend std::istream& operator>>(std::istream& is, Personne& personne);

    // Op�rateur de flux de sortie
    friend std::ostream& operator<<(std::ostream& os, const Personne& personne);

    protected:
        // Membres prot�g�s accessibles par les classes d�riv�es
        std::string nom;
        std::string prenom;

    private:

};

#endif // PERSONNE_H
