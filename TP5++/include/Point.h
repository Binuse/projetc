#ifndef POINT_H
#define POINT_H


class Point
{
    public:
        Point();
        Point(int abs = 0, int ord = 0);
        virtual ~Point();
        int getX() const;
        int getY() const;
        void deplacerDe(int dx, int dy);
        void deplacerVers(int x, int y);
        static int getNbPoints();

    protected:

    private:
        int x;
        int y;
        static int NbPoints;
};

#endif // POINT_H
