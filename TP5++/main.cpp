#include <iostream>
#include <vector>
#include "personne.h"
#include "etudiant.h"
#include "boursier.h"

using namespace std;

int main()
{

    //Exercice 2 : Liste h�t�rog�ne
    Personne *tab[10] = {
        new Personne("Hugo", "Victor"),
        new Etudiant("Babbit", "Charlie", "123"),
        new Boursier("Freeman", "Gordon", "1234", "AZ123P")
     };

     // Affichage des �l�ments du tableau
     //utilisation de la methode afficher
    /*for (int i = 0; i < 10; ++i) {
        if (tab[i] != nullptr) {
            tab[i]->afficher();
        }
    }*/

    // Lib�ration de la m�moire allou�e dynamiquement
    for (int i = 0; i < 10; ++i) {
        if (tab[i] != nullptr) {
            delete tab[i];
            tab[i] = nullptr;
        }
    }


    //Exercice 3
    //D�clation d'un vecteur de pointeurs vers des objets de type Personne
    /*std::vector<Personne*> listePersonnes;

    //D�claration d'un menu
    while (true) {
        std::cout << "\nMenu :\n";
        std::cout << "1. Ajouter une Personne\n";
        std::cout << "2. Ajouter un Etudiant\n";
        std::cout << "3. Ajouter un Boursier\n";
        std::cout << "4. Afficher la liste\n";
        std::cout << "5. Quitter\n";
        std::cout << "Choix : ";

        int choix;
        std::cin >> choix;

        switch (choix) {
            case 1: {
                //Aout d'une Personne � la liste
                Personne* personne = new Personne();
                personne->lire(std::cin);//saisie des donn�es de la personne
                listePersonnes.push_back(personne);
                break;
            }
            case 2: {
                //Ajout d'un Etudiant � la liste
                Etudiant* etudiant = new Etudiant();
                etudiant->lire(std::cin);//Saisie des donn�es d'un �tudiant
                listePersonnes.push_back(etudiant);
                break;
            }
            case 3: {
                //Ajout d'une Personne � la liste
                Boursier* boursier = new Boursier();
                boursier->lire(std::cin);//Saisie des donn�es d'un boursier
                listePersonnes.push_back(boursier);
                break;
            }
            case 4:
                // Affichage de la liste des personnes
                std::cout << "\nListe des personnes :\n";
                for (const auto& personne : listePersonnes) {
                    personne->ecrire(std::cout);
                    std::cout << std::endl;
                }
                break;
            case 5:
                // Lib�ration de la m�moire allou�e dynamiquement
                for (const auto& personne : listePersonnes) {
                    delete personne;
                }
                return 0;
            default:
                std::cout << "Choix invalide. Veuillez r�essayer.\n";
        }
    }*/
    //Ecercice 4
    std::vector<Personne*> listePersonnes1;  // Modification du nom de la liste

    while (true) {
        std::cout << "\nMenu :\n";
        std::cout << "1. Ajouter une Personne\n";
        std::cout << "2. Ajouter un Etudiant\n";
        std::cout << "3. Ajouter un Boursier\n";
        std::cout << "4. Afficher la liste\n";
        std::cout << "5. Quitter\n";
        std::cout << "Choix : ";

        int choix;
        std::cin >> choix;

        switch (choix) {
            case 1: {
                Personne* personne = new Personne();
                std::cin >> *personne;
                listePersonnes1.push_back(personne);
                break;
            }
            case 2: {
                Etudiant* etudiant = new Etudiant();
                std::cin >> *etudiant;
                listePersonnes1.push_back(etudiant);
                break;
            }
            case 3: {
                Boursier* boursier = new Boursier();
                std::cin >> *boursier;
                listePersonnes1.push_back(boursier);
                break;
            }
            case 4:
                std::cout << "\nListe des personnes :\n";
                for (const auto& personne : listePersonnes1) {
                    std::cout << *personne << std::endl;
                }
                break;
            case 5:
                // Lib�ration de la m�moire allou�e dynamiquement
                for (const auto& personne : listePersonnes1) {
                    delete personne;
                }
                return 0;
            default:
                std::cout << "Choix invalide. Veuillez r�essayer.\n";
        }
    }

    return 0;
}


