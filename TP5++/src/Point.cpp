#include "Point.h"

Point::Point(int abs, int ord) : x(abs), y(ord) {
    ++NbPoints;
}

Point::~Point()
{
    //dtor
}

int Point::NbPoints = 0;

int Point::getX() const {
    return x;
}

int Point::getY() const {
    return y;
}

void Point::deplacerDe(int dx, int dy) {
    x += dx;
    y += dy;
}

void Point::deplacerVers(int newX, int newY) {
    x = newX;
    y = newY;
}

int Point::getNbPoints() {
    return NbPoints;
}
