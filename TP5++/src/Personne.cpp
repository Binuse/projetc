#include <iostream>
#include "Personne.h"

//Constructeur par d�faut
Personne::Personne()
{
    nom = "";
    prenom = "";
    std::cout << "Personne::Personne()" << std::endl;
}

//Constructeur avec param�tre
Personne::Personne(const std::string& _nom, const std::string& _prenom) : nom(_nom), prenom(_prenom) {
    std::cout << "Personne::Personne() avec param�tres" << std::endl;
}
//Destructeur
Personne::~Personne()
{
    std::cout << "Personne::~Personne()" << std::endl;
}

void Personne::afficher() const {
    std::cout << "Nom: " << nom << ", Pr�nom: " << prenom << std::endl;
}

// M�thode pour lire les informations d'une personne depuis une entr�e
void Personne::lire(std::istream& is) {
    is >> nom >> prenom;
}

// M�thode pour �crire les informations d'une personne dans une sortie
void Personne::ecrire(std::ostream& os) const {
    os << "Personne(" << nom << "," << prenom << ")";
}


// Op�rateur de flux d'entr�e
std::istream& operator>>(std::istream& is, Personne& personne) {
    personne.lire(is);
    return is;
}

// Op�rateur de flux de sortie
std::ostream& operator<<(std::ostream& os, const Personne& personne) {
    personne.ecrire(os);
    return os;
}
