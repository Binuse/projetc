#include "Etudiant.h"

// Constructeur par d�faut de Boursier
Etudiant::Etudiant(): Personne()
{
    numeroINE = "";
    std::cout << "Etudiant::Etudiant()" << std::endl;
}

// Constructeur avec param�tres
Etudiant::Etudiant(const std::string& _nom, const std::string& _prenom, const std::string& _numeroINE)
    : Personne(_nom, _prenom), numeroINE(_numeroINE)
{
    std::cout << "Etudiant::Etudiant() avec param�tres" << std::endl;
}
//Destructeur
Etudiant::~Etudiant()
{
     std::cout << "Etudiant::~Etudiant()" << std::endl;
}
void Etudiant::afficher() const {
    Personne::afficher(); // Appel � la fonction d'affichage de la classe de base
    std::cout << "Num�ro INE: " << numeroINE << std::endl;
}

// M�thode pour lire les informations d'un �tudiant depuis une entr�e
void Etudiant::lire(std::istream& is) {
    Personne::lire(is); // Appel � la fonction de lecture de la classe de base
    is >> numeroINE;
}

void Etudiant::ecrire(std::ostream& os) const {
    os << "Etudiant("; // D�but de l'�criture de la repr�sentation de l'objet
    Personne::ecrire(os); // Appel � la fonction d'�criture de la classe de base
    os << "," << numeroINE << ")"; // Ajout du num�ro INE � la repr�sentation
}
