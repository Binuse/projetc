#include "Boursier.h"

// Constructeur par d�faut de Boursier
Boursier::Boursier() : Etudiant() {
    std::cout << "Boursier::Boursier()" << std::endl;
    lire(std::cin);  // Appel de la fonction lire pour �viter la duplication du code
}
// Constructeur avec param�tres
Boursier::Boursier(const std::string& _nom, const std::string& _prenom, const std::string& _numeroINE, const std::string& _numeroDossierBourse)
: Etudiant(_nom, _prenom, _numeroINE), numeroDossierBourse(_numeroDossierBourse)
{
    std::cout << "Boursier::Boursier() avec param�tres" << std::endl;
}

//Destructeur
Boursier::~Boursier()
{
    std::cout << "Boursier::~Boursier()" << std::endl;
}
void Boursier::afficher() {
    Etudiant::afficher(); // Appel � la fonction d'affichage de la classe de base
    std::cout << "Num�ro de dossier de bourse: " << numeroDossierBourse << std::endl;
}

// M�thode pour lire les informations d'un boursier depuis une entr�e
void Boursier::lire(std::istream& is) {
    Etudiant::lire(is); // Appel � la fonction de lecture de la classe de base (Etudiant)
    is >> numeroDossierBourse;
}

// M�thode pour �crire les informations d'un boursier dans une sortie
void Boursier::ecrire(std::ostream& os) const {
    os << "Boursier("; // D�but de l'�criture de la repr�sentation de l'objet
    Etudiant::ecrire(os); // Appel � la fonction d'�criture de la classe de base (Etudiant)
    os << "," << numeroDossierBourse << ")"; // Ajout du num�ro de dossier de bourse � la repr�sentation
}
